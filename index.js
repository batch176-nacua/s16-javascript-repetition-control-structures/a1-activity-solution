let num = parseInt(prompt("Enter a number: "))
console.log("The number you provided is " +num)

for(let x=num; x>0; x--){

	if(x <= 50){
		console.log("The current value is at " +x+ ". Terminating the loop.")
		break;
	}

	if(x % 10 ==0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if(x % 5 == 0){
		console.log(x)
	}
}

let word = "supercalifragilisticexpialidocious";
let consonant = "";

console.log(word)

for(let i=0; i < word.length; i++){
	
	if(word[i].toLowerCase() == "a" || word[i].toLowerCase() == "e" || 
			word[i].toLowerCase() == "i" || word[i].toLowerCase() == "o" ||
							word[i].toLowerCase() == "u")
	{
		continue;
	}else{
		consonant = consonant.concat(word[i]);
	}	
}
console.log(consonant)